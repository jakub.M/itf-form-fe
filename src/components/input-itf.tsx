import React from 'react';
import { TextField } from '@material-ui/core';
import { useFormContext } from 'react-hook-form';

export const InputItf = ({ name, label, type }: any) => {
  const { register, errors } = useFormContext();
  const errorHandler = errors[name]
    ? { error: true, helperText: errors[name].message }
    : {};
  return (
    <TextField
      {...{ label, name, type, inputRef: register, ...errorHandler }}
    />
  );
};
