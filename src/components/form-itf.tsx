import React from 'react';
import { FormContext } from 'react-hook-form';
import { Button } from '@material-ui/core';
import { InputItf } from './input-itf';
import { FormValidation } from 'types';
import { startCase } from 'lodash';

export const FormItf = ({ config, handler, children, Input }: any) => {
  const { methods, onSubmit, metatype } = handler();
  const context =
    children ??
    FormValidation.getFormSchema(metatype)?.map(
      ({ name, label, placeholder, type }: any) => {
        const params = {
          name,
          label: label ?? startCase(name),
          type,
          placeholder,
        }; // match with config params
        return Input ? <Input {...params} /> : <InputItf {...params} />;
      }
    );

  return (
    <FormContext {...methods}>
      <form onSubmit={onSubmit}>
        {context}
        <Button type="submit" variant="contained" color="primary">
          Submit
        </Button>
      </form>
    </FormContext>
  );
};
