import axios, { Method } from 'axios';
import { useState } from 'react';
import { includes } from 'lodash';
import { MsgResutl } from 'types';
import { useForm } from 'react-hook-form';
import cogoToast from 'cogo-toast';

export const apiUrl = 'http://localhost:3001';
//export const apiUrl = 'https://api.kybox.dev2.itdotfocus.com';

interface ApiProps<T> {
  method: Method;
  path: string;
  msgResult?: MsgResutl;
  data?: T;
  onSuccess?: Function;
  withLoading?: boolean;
}
interface UseApiProps {
  validationSchema?: any;
  defaultValues?: any;
}
export const useApi = (props?: UseApiProps) => {
  const [loading, setLoading] = useState(false);
  const methods = useForm({ ...props });

  const api = async <T>({
    method,
    path,
    msgResult,
    data,
    onSuccess,
  }: ApiProps<T>) => {
    setLoading(true);
    await axios({
      withCredentials: true,
      method,
      url: apiUrl + path,
      data,
      timeout: 4000,
    })
      .then((res) => {
        if (includes([MsgResutl.Both, MsgResutl.Success], msgResult)) {
          cogoToast.success('Success');
        }
        onSuccess?.(res);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        if (includes([MsgResutl.Both, MsgResutl.Failure], msgResult)) {
          cogoToast.error('error');
        }
      });
  };

  return { api, loading, methods };
};
