import { FormValidation, MsgResutl } from 'types';
import { useApi } from './useApi';

export const FormHandler = (
  metatype: any,
  path: string,
  msgResult?: MsgResutl,
  onSuccess?: Function
) => {
  const { api, methods, loading } = useApi({
    validationSchema: FormValidation.getSchema(metatype),
  });

  const onSubmit = methods.handleSubmit((data: any) => {
    api({
      method: 'Post',
      path,
      msgResult,
      data,
      onSuccess,
    } as any);
  });

  return { methods, onSubmit, loading, metatype };
};
