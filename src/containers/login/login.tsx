import React from 'react';
import { FormItf } from '../../components/form-itf';
import { LoginHandler } from './login.handler';

export const Login = () => <FormItf {...{ handler: LoginHandler }} />;
